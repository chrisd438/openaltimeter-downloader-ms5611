package org.openaltimeter.desktopapp;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.openaltimeter.data.HeightUnits;

@SuppressWarnings("serial")
public class DLGAnalysisDialog extends JDialog {
	
	private static final String PREFS_MARK_LAUNCH_HEIGHTS = "PREFS_MARK_LAUNCH_HEIGHTS";
	private static final String PREFS_MARK_MAX_HEIGHTS = "PREFS_MARK_MAX_HEIGHTS";
	private static final String PREFS_CORRECT_BASELINE = "PREFS_CORRECT_BASELINE";
	private static final String PREFS_SHOW_STATISTICS = "PREFS_SHOW_STATISTICS";
	private static final String PREFS_LIMIT_BY_HEIGHT = "PREFS_LIMIT_BY_HEIGHT";
	private static final String PREFS_LIMIT_BY_TIME = "PREFS_LIMIT_BY_TIME";

	private JCheckBox chckbxShowStatistics;
	private JCheckBox chckbxCorrectBaselineFor;
	private JCheckBox chckbxMarkMaximumHeights;
	private JCheckBox chckbxMarkLaunchHeights;
	private JCheckBox chckbxLimitByHeight;
	private JCheckBox chckbxLimitByTime;
	
	private JTextField heightCutoffTextField;
	private JTextField timeCutoffTextField;
	private JLabel lblHeightUnits;
	private JLabel lblTimeUnits;
	
	public JTextField getHeightCutoff() {
		return heightCutoffTextField;
	}
	public JTextField getTimeCutoff() {
		return timeCutoffTextField;
	}
	
	private boolean success = false;
	Preferences prefs;
	
	public boolean shouldLimitAnalysisByHeight() {
		return chckbxLimitByHeight.isSelected();
	}
	
	public void setLimitAnalysisByHeight(boolean limitByHeights) {
		this.chckbxLimitByHeight.setSelected(limitByHeights);
	}
	
	public boolean shouldLimitAnalysisByTime() {
		return chckbxLimitByTime.isSelected();
	}
	
	public void setLimitAnalysisByTime(boolean limitByTime) {
		this.chckbxLimitByTime.setSelected(limitByTime);
	}

	public boolean shouldMarkLaunchHeights() {
		return chckbxMarkLaunchHeights.isSelected();
	}

	public void setMarkLaunchHeights(boolean markLaunchHeights) {
		this.chckbxMarkLaunchHeights.setSelected(markLaunchHeights);
	}

	public boolean shouldMarkMaxHeights() {
		return chckbxMarkMaximumHeights.isSelected();
	}

	public void setMarkMaxHeights(boolean markMaxHeights) {
		this.chckbxMarkMaximumHeights.setSelected(markMaxHeights);
	}

	public boolean shouldCorrectBaseline() {
		return chckbxCorrectBaselineFor.isSelected();
	}

	public void setCorrectBaseline(boolean correctBaseline) {
		this.chckbxCorrectBaselineFor.setSelected(correctBaseline);
	}

	public boolean shouldShowStatistics() {
		return chckbxShowStatistics.isSelected();
	}

	public void setShowStatistics(boolean showStatistics) {
		this.chckbxShowStatistics.setSelected(showStatistics);
	}
	
	public DLGAnalysisDialog(final Controller controller, HeightUnits heightUnits) {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(DLGAnalysisDialog.class.getResource("/logo_short_64.png")));
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("DLG flight analysis");
		setBounds(100, 100, 282, 235);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 358, 526, 10);
		getContentPane().add(panel);
		
		chckbxMarkLaunchHeights = new JCheckBox("Mark launch heights");
		chckbxMarkLaunchHeights.setSelected(true);
		chckbxMarkLaunchHeights.setBounds(6, 7, 180, 23);
		getContentPane().add(chckbxMarkLaunchHeights);
		
		chckbxMarkMaximumHeights = new JCheckBox("Mark maximum heights");
		chckbxMarkMaximumHeights.setSelected(true);
		chckbxMarkMaximumHeights.setBounds(6, 33, 153, 23);
		getContentPane().add(chckbxMarkMaximumHeights);
		
		chckbxCorrectBaselineFor = new JCheckBox("Correct baseline for weather shifts");
		chckbxCorrectBaselineFor.setSelected(true);
		chckbxCorrectBaselineFor.setBounds(6, 59, 250, 23);
		getContentPane().add(chckbxCorrectBaselineFor);
		
		chckbxShowStatistics = new JCheckBox("Show launch statistics window");
		chckbxShowStatistics.setSelected(true);
		chckbxShowStatistics.setBounds(6, 85, 180, 23);
		getContentPane().add(chckbxShowStatistics);
		
		chckbxLimitByHeight = new JCheckBox("Do not count launches under");
		chckbxLimitByHeight.setSelected(true);
		chckbxLimitByHeight.setBounds(6, 111, 164, 23);
		getContentPane().add(chckbxLimitByHeight);
		
		heightCutoffTextField = new JTextField();
		heightCutoffTextField.setText("20");
		heightCutoffTextField.setColumns(10);
		heightCutoffTextField.setBounds(170, 111, 30, 23);
		getContentPane().add(heightCutoffTextField);
		
		String label;
		switch (heightUnits) {
		case METERS:
			 label = "m";
			 break;
		case FT:
		default:
			label = "ft";
			break;
		}
		lblHeightUnits = new JLabel(label);
		lblHeightUnits.setBounds(205, 111, 30, 23);
		getContentPane().add(lblHeightUnits);
		
		chckbxLimitByTime = new JCheckBox("Do not count flights under");
		chckbxLimitByTime.setSelected(true);
		chckbxLimitByTime.setBounds(6, 137, 164, 23);
		getContentPane().add(chckbxLimitByTime);
		
		timeCutoffTextField = new JTextField();
		timeCutoffTextField.setText("20");
		timeCutoffTextField.setColumns(10);
		timeCutoffTextField.setBounds(170, 137, 30, 23);
		getContentPane().add(timeCutoffTextField);
		
		lblTimeUnits = new JLabel("s");
		lblTimeUnits.setBounds(205, 137, 30, 23);
		getContentPane().add(lblTimeUnits);
		
		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				success = true;
				prefs.putBoolean(PREFS_MARK_LAUNCH_HEIGHTS, shouldMarkLaunchHeights());			
				prefs.putBoolean(PREFS_MARK_MAX_HEIGHTS, shouldMarkMaxHeights());			
				prefs.putBoolean(PREFS_CORRECT_BASELINE, shouldCorrectBaseline());			
				prefs.putBoolean(PREFS_SHOW_STATISTICS, shouldShowStatistics());
				prefs.putBoolean(PREFS_LIMIT_BY_HEIGHT, shouldShowStatistics());
				prefs.putBoolean(PREFS_LIMIT_BY_TIME, shouldShowStatistics());
				dispose();
			}
		});
		btnRun.setBounds(97, 168, 89, 23);
		getContentPane().add(btnRun);

		prefs = Preferences.userNodeForPackage(this.getClass());
		setMarkLaunchHeights(prefs.getBoolean(PREFS_MARK_LAUNCH_HEIGHTS, true));
		setMarkMaxHeights(prefs.getBoolean(PREFS_MARK_MAX_HEIGHTS, true));
		setCorrectBaseline(prefs.getBoolean(PREFS_CORRECT_BASELINE, true));
		setShowStatistics(prefs.getBoolean(PREFS_SHOW_STATISTICS, true));
		setLimitAnalysisByHeight(prefs.getBoolean(PREFS_LIMIT_BY_HEIGHT, false));
		setLimitAnalysisByTime(prefs.getBoolean(PREFS_LIMIT_BY_TIME, false));
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
	}

	public boolean isSuccessful() {
		return success;
	}

}
