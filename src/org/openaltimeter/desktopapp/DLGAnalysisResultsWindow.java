package org.openaltimeter.desktopapp;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.openaltimeter.data.AltitudeConverter;
import org.openaltimeter.data.HeightUnits;
import org.openaltimeter.data.analysis.DLGFlight;
import org.openaltimeter.data.analysis.StatSet;

@SuppressWarnings("serial")
public class DLGAnalysisResultsWindow extends JFrame {

	private JPanel contentPane;
	private HistogramDataset heightHDS;
	private HistogramDataset timeHDS;
	private JFreeChart chart;
	List<DLGFlight> flights;
	HeightUnits units;
	private DescriptiveStatistics ds;
	private String heightStatisticsText;
	private String timesStatisticsText;
	private String unitString;
	private float heightCutoff;
	private float timeCutoff;
	
	static final Color TIMES_COLOR = new Color(255, 0, 0);

	public DLGAnalysisResultsWindow(List<DLGFlight> flights, HeightUnits units, float heightCutoff, float timeCutoff) {
		setResizable(false);
		this.flights = flights;
		this.units = units;
		if (units == HeightUnits.FT) unitString = "ft";
		else unitString = "m";
		this.heightCutoff = heightCutoff;
		this.timeCutoff = timeCutoff;
		prepareData();
		makeGUI();
	}

	private void makeGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DLGAnalysisDialog.class.getResource("/logo_short_64.png")));
		setTitle("DLG flight analysis");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 660, 480);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
				
		chart = ChartFactory.createHistogram(null,
				"Height (" + unitString + ")", 
				"Frequency", 
				heightHDS, 
				PlotOrientation.VERTICAL, 
				false, 
				true, 
				false);
		final XYPlot plot = chart.getXYPlot();
        plot.setDomainGridlinesVisible(false);
        plot.setRangeGridlinesVisible(false);
        plot.setBackgroundPaint(AltimeterChart.BG_COLOR);
             
		NumberAxis heightAxisD = new NumberAxis("Launch height (" + unitString + ")");
		NumberAxis timesAxisD = new NumberAxis("Flight lengths (s)");
		heightAxisD.setTickMarkPaint(AltimeterChart.PRESSURE_COLOR);
		heightAxisD.setTickLabelPaint(AltimeterChart.PRESSURE_COLOR);
		heightAxisD.setAxisLinePaint(AltimeterChart.PRESSURE_COLOR);
		heightAxisD.setLabelPaint(AltimeterChart.PRESSURE_COLOR);
		timesAxisD.setTickMarkPaint(TIMES_COLOR);
		timesAxisD.setTickLabelPaint(TIMES_COLOR);
		timesAxisD.setAxisLinePaint(TIMES_COLOR);
		timesAxisD.setLabelPaint(TIMES_COLOR);
		heightAxisD.setAutoRange(true);
		timesAxisD.setAutoRange(true);
        plot.setDomainAxis(0,heightAxisD);
        plot.setDomainAxis(1,timesAxisD);
        plot.setDomainAxisLocation(0, AxisLocation.BOTTOM_OR_RIGHT);
        plot.setDomainAxisLocation(1, AxisLocation.BOTTOM_OR_RIGHT);
        plot.setDataset(0, heightHDS);
        plot.setDataset(1, timeHDS);
        plot.mapDatasetToDomainAxis(0, 0);
        plot.mapDatasetToDomainAxis(1, 1);
        
		NumberAxis axisR = new NumberAxis("Frequency");
		axisR.setTickUnit(new NumberTickUnit(1));
		axisR.setAutoRange(true);
        plot.setRangeAxis(axisR);
        
		XYBarRenderer renderer = new XYBarRenderer();
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);
		
		XYBarRenderer renderer2 = new XYBarRenderer();
		renderer2.setBarPainter(new StandardXYBarPainter());
		renderer2.setShadowVisible(false);
		
        renderer.setSeriesPaint(0, new Color(0, 0, 255, 100));
        plot.setRenderer(0,renderer);
        renderer2.setSeriesPaint(0, new Color(255, 0, 0, 100));
        plot.setRenderer(1,renderer2);
        
		ChartPanel cp = new ChartPanel(chart);
		cp.setBounds(0, 0, 414, 456);
		contentPane.add(cp);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(414, 0, 244, 456);
		textArea.setEditable(false);
		textArea.setText(heightStatisticsText + timesStatisticsText);
		textArea.setFont(new Font(Font.MONOSPACED,Font.PLAIN, 12));
		contentPane.add(textArea);
	}
	
	void prepareData() {
		
		// extract launch height data
		StatSet heights = new StatSet();
		for (int i = 0; i < flights.size(); i++) {
			if(units == HeightUnits.FT) {
				if ( AltitudeConverter.feetFromM(flights.get(i).launchHeight) > heightCutoff ) {
					heights.add( AltitudeConverter.feetFromM(flights.get(i).launchHeight));
				}
			}
			else {
				if ( flights.get(i).launchHeight > heightCutoff ) {
					heights.add(flights.get(i).launchHeight);
				}
			}
		}
		
		// extract times
		StatSet times = new StatSet();
		for (int i = 0; i < flights.size(); i++) {
			if(units == HeightUnits.FT) {
				if ( AltitudeConverter.feetFromM(flights.get(i).flightLength) > timeCutoff )
					times.add(AltitudeConverter.feetFromM(flights.get(i).flightLength));
			}
			else {
				if ( flights.get(i).flightLength > timeCutoff ) {
					times.add(flights.get(i).flightLength);
				}
			}
		}
		
		// prepare stats
		NumberFormat df = DecimalFormat.getInstance();
		df.setMaximumFractionDigits(1);	
		heightStatisticsText = "Number of launches over " + df.format(heightCutoff) + " " + 
				unitString + ": " + df.format(heights.size()) + "\n" +
				"Mean launch height: " + df.format(heights.mean()) + " " + unitString + "\n" +
				"Standard deviation: " + df.format(heights.stdDev()) + " " + unitString + "\n" +
				"Median launch height: " + df.format(heights.median()) + " " + unitString + "\n" +
				"Max launch height: " + df.format(heights.max()) + " " + unitString + "\n\n";
		timesStatisticsText = "Number of flights over " + df.format(timeCutoff) + " s: " + 
				df.format(times.size()) + "\n" +
				"Mean flight length: " + df.format(times.mean()) + " s\n" +
				"Standard deviation: " + df.format(times.stdDev()) + " s\n" +
				"Median flight length: " + df.format(times.median()) + " s\n" +
				"Max flight length: " + df.format(times.max()) + " s\n";

		// prepare
		heightHDS = new HistogramDataset();
		heightHDS.setType(HistogramType.FREQUENCY);
		int numBins = 30;//(int)Math.round(ds.getMax() - ds.getMin());
		heightHDS.addSeries("Launches", heights.getData(), numBins);
		timeHDS = new HistogramDataset();
		timeHDS.addSeries("Times", times.getData(), 30);
	}

}
