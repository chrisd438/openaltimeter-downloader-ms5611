package org.openaltimeter.data.analysis;

public class DLGFlight {
	
	public int startIndex;
	public double startHeight;
	public int launchIndex;
	public double launchHeight;
	public double launchWindowEndHeight;
	public int maxIndex;
	public double maxHeight;
	public double flightLength;
	public int endIndex;
	
	public String rawDataToString()
	{
		return "StIx: " + startIndex 
		+ " LchHIx: " + launchIndex 
		+ " MaxHIx: " + maxIndex
		+ " EndIx: " + endIndex 
		+ " StH: " + startHeight 
		+ " LH: " + launchHeight 
		+ " LWEH: " + launchWindowEndHeight 
		+ " MaxH: " + maxHeight 
		+ " FL: " + flightLength;
	}
}
