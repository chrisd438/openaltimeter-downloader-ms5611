package org.openaltimeter.data.analysis;

//StatSet.java
//Simple object for single-variable descriptive statistics

import java.util.ArrayList;
import java.util.Collections;

public class StatSet{

	private ArrayList<Double> data; // instance variable--a list of floats

	public StatSet(){     // constructor
		data = new ArrayList<Double>();
	}

	public void add(double x){
		// adds x to this StatSet
		data.add(x);
	}
	
	public double[] getData() {
		double[] dataTemp = new double[this.size()];
		for (int i = 0; i < this.size(); i++) dataTemp[i] = data.get(i);
		return dataTemp;
	}

	public double mean(){
		// returns the mean of the values in this StatSet
		// pre: size() >= 1
		double sum = 0.0;
		for(double x:data){
			sum += x;
		}
		return sum / data.size();
	}

	public double stdDev(){
		// returns the standard deviation of the values in this StatSet
		// pre: size() >= 2
		double xbar = mean();
		double devSq = 0.0;
		for(double x:data){
			double dev = x - xbar;
			devSq += dev * dev;
		}
		if (data.size() - 1 == 0 ) {
			return 0;
		}
		else {
			return Math.sqrt(devSq/(data.size()-1));
		}
	}

	public double median(){
		// returns median of values in this StatSet
		// pre: size() >= 1
		Collections.sort(data);
		int n = data.size();
		int mid = n / 2;
		if (n%2 == 0){
			return (data.get(mid) + data.get(mid-1))/2.0;
		}
		else{
			return data.get(mid);
		}
	}

	public double max() {
		// returns max of values in this StatSet
		double maxVal = -1000;
		for(double x:data){
			if (x > maxVal) maxVal = x;
		}
		return maxVal;
	}
	public int size(){
		return data.size();
	}
}   
