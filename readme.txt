/******************************************/
Openaltimeter MS5611 Downloader

Notes:

OA Downloader pre-packaged with V8.MS5611.1 firmware for direct upload.

Incorporated various other fixes/patches from other OA downloader contributors,
including SmallJoeMan


/******************************************/
